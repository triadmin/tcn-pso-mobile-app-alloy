/**
 * Push notification class
 * 
 * @class push
 * @uses core
 */
var APP = require("core");
var UTIL = require("utilities");

/**
 * The device token
 * @type String
 */
exports.deviceToken = null;

/**
 * Initializes the push notifications based on selected vendor
 * @param {Function} _callback The callback to run after device registration
 */
var vendorInit = function(_callback) {
	if(APP.Settings.notifications.provider === "ACS") {
		require("push/acs").registerDevice(_callback);
	}

	if(APP.Settings.notifications.provider === "UA") {
		require("push/ua").registerDevice(_callback);
	}
};

/**
 * Registers the app for push notifications
 */
exports.init = function() {
	APP.log("debug", "PUSH.init");

	vendorInit(function() {
		APP.log("debug", "PUSH.init @success");
	});
};

var deepLinkFromPush = function(_data) {
	// See if we can find this page in database.
	var tabs = APP.Nodes;
	var e = {};
	for(a = 1; a <= tabs.length - 1; a++) {
		var index = tabs[a].index;
		var type = tabs[a].type;
		var id = tabs[a].id;
		if(type == 'article' && id == _data.section_id) {
			var db = Ti.Database.open("ChariTi");
			var sql = "SELECT * FROM article_" + index + " WHERE title LIKE '%" + _data.page_title + "%' LIMIT 1;";
			var data = db.execute(sql);
			var article_id = data.fieldByName('id');
			db.close();

			APP.addChild("article_article", {
				id: article_id,
				index: index
			});

		}
	}
};

/**
 * The function to run after a push has been received
 * @param {Object} _data The push data received
 */
exports.pushRecieved = function(_data) {
	APP.log("debug", "ACS.pushReceived");
	APP.log("trace", JSON.stringify(_data));

	setTimeout(deepLinkFromPush(_data), 2000);

	var payload = null;

	if(_data.data) {
		payload = _data.data;
	} else if(_data.payload) {
		payload = JSON.parse(_data.payload);
		payload.alert = payload.android.alert;
	} else {
		return;
	}

	var dialog = Ti.UI.createAlertDialog({
		title: "Push Notification",
		message: payload.alert,
		buttonNames: ["OK", "Cancel"],
		cancel: 1
	});

	dialog.show();
};